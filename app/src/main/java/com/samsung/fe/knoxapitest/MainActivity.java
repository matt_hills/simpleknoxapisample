package com.samsung.fe.knoxapitest;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.Toast;

import com.samsung.fe.knoxapitest.receivers.AdminReceiver;
import com.samsung.fe.knoxapitest.ui.AppActivation;
import com.samsung.fe.knoxapitest.utility.KnoxApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_toggle)
    Button btnToggle;
    @BindView(R.id.btn_get_state) Button btnGetState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        ButterKnife.bind(this);

        if(!isDPCActive()) {
            launchAppActivation();
        }

    }

    private void launchAppActivation() {
        Intent intent = new Intent(this, AppActivation.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_toggle)
    public void lockDevice() {

        Boolean knoxAPIResult = KnoxApi.toggleFirmware();

        Toast.makeText(this, "FirmwareRecovery is: " + knoxAPIResult, Toast.LENGTH_SHORT).show();
    }


    private boolean isDPCActive() {
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName mAdminName = new ComponentName(this, AdminReceiver.class);

        return devicePolicyManager.isAdminActive(mAdminName);
    }
}
