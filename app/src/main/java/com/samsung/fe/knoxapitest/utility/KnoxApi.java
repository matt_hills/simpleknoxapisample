package com.samsung.fe.knoxapitest.utility;

import android.util.Log;

import com.samsung.android.knox.EnterpriseDeviceManager;
import com.samsung.android.knox.restriction.RestrictionPolicy;
import com.samsung.fe.knoxapitest.App;

import static com.samsung.fe.knoxapitest.Constants.LOG_TAG;

public class KnoxApi {

    public static boolean toggleFirmware() {
        EnterpriseDeviceManager edm = EnterpriseDeviceManager.getInstance(App.appCtx);
        RestrictionPolicy restrictionPolicy = edm.getRestrictionPolicy();

        Boolean firmwareRecoveryState = restrictionPolicy.isFirmwareRecoveryAllowed(true);


        try {
            //flip the state of allowFirmwareRecovery
            if (firmwareRecoveryState) {
                restrictionPolicy.allowFirmwareRecovery(false);

            } else {
                restrictionPolicy.allowFirmwareRecovery(true);
            }

        } catch (SecurityException se) {
            Log.d(LOG_TAG, "Failed to call Knox APIs: " + se);
        }

        return firmwareRecoveryState;

    }
}
