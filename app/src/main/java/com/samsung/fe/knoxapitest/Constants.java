package com.samsung.fe.knoxapitest;

public class Constants {

    /*
     * Add your Knox KPE (AKA SLK) key
     * from https://seap.samsung.com/license-keys
     * Reminder: You will need to login
     */

    public static final String KNOX_LICENSE = null;

    public static final String LOG_TAG = "APITest";

    public static final int DEVICE_ADMIN_ADD_RESULT_ENABLE = 1;

}
