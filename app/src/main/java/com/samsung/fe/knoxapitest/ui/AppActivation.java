package com.samsung.fe.knoxapitest.ui;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import com.samsung.android.knox.license.KnoxEnterpriseLicenseManager;
import com.samsung.fe.knoxapitest.Constants;
import com.samsung.fe.knoxapitest.R;
import com.samsung.fe.knoxapitest.receivers.AdminReceiver;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.samsung.fe.knoxapitest.Constants.DEVICE_ADMIN_ADD_RESULT_ENABLE;

public class AppActivation extends AppCompatActivity {

    private ComponentName mDeviceAdmin;

    @BindView(R.id.btn_admin_activation)
    Button btnAdminActivation;
    @BindView(R.id.btn_admin_deactivation) Button btnAdminDeactivation;
    @BindView(R.id.btn_license_activation) Button btnLicenseActivation;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_admin_activation)
    public void ActivateAdmin() {

        mDeviceAdmin = new ComponentName(AppActivation.this, AdminReceiver.class);
        // Ask the user to add a new device administrator to the system
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdmin);
        // Start the add device admin activity
        startActivityForResult(intent, DEVICE_ADMIN_ADD_RESULT_ENABLE);
    }

    @OnClick(R.id.btn_admin_deactivation)
    public void DeactivateAdmin() {

        //logView(getResources().getString(R.string.deactivating_admin));
        DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (dpm != null) {
            // Deactivate this application as device administrator
            dpm.removeActiveAdmin(new ComponentName(this, AdminReceiver.class));

        }
    }

    @OnClick(R.id.btn_license_activation)
    public void activateSKLLicense() {
        // Instantiate the KnoxEnterpriseLicenseManager class to use the activateLicense method
        KnoxEnterpriseLicenseManager sklManager = KnoxEnterpriseLicenseManager.getInstance(this);
        Toast.makeText(this, "Attempting License Activation...", Toast.LENGTH_SHORT).show();

        if (Constants.KNOX_LICENSE != null) {
            try {
                // SKL License Activation TODO Add SKL license key to Constants.java
                sklManager.activateLicense(Constants.KNOX_LICENSE);
                //mUtils.log(getResources().getString(R.string.klm_license_progress));

            } catch (Exception e) {
                //mUtils.processException(e, TAG);
            }
        } else {
            Toast.makeText(this, "Please add SLK to Constants!", Toast.LENGTH_SHORT).show();
        }

    }

}
